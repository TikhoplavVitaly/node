FROM centos:centos7

RUN echo "Europe/Moscov" > /etc/timezone
RUN localedef -i ru_RU -f UTF-8 ru_RU.UTF-8

RUN groupadd -r node --gid=1000; \
	useradd -r -g node --uid=1000 --home-dir=/home/node --shell=/bin/bash node; \
	mkdir -p /home/node; \
	chown -R node:node /home/node

ENV GOSU_VERSION 1.10
RUN set -x && \
    curl -Lo /usr/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" && \
    curl -Lo /tmp/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64.asc" && \
    export GNUPGHOME="$(mktemp -d)" && \
    for server in $(shuf -e ha.pool.sks-keyservers.net \
                            hkp://p80.pool.sks-keyservers.net:80 \
                            keyserver.ubuntu.com \
                            hkp://keyserver.ubuntu.com:80 \
                            pgp.mit.edu) ; do \
        gpg --keyserver "$server" --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && break || : ; \
    done && \
    gpg --batch --verify /tmp/gosu.asc /usr/bin/gosu && \
    rm -r "$GNUPGHOME" /tmp/gosu.asc && \
    chmod +x /usr/bin/gosu && \
    # Allow user to become root
    #chmod u+s /usr/bin/gosu && \
    gosu nobody true

RUN yum install -y curl \
	&& curl --silent --location https://rpm.nodesource.com/setup_11.x | bash \
	&& curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo \
	&& rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg \
	&& yum install -y nodejs yarn \
	&& yum clean all

USER node